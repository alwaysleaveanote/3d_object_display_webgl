﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraBackground : MonoBehaviour
{
    List<string> colorNames = new List<string>() { "Tan","White","Gray","Blue","Green", "Yellow","Red"};
    List<Color> colors;

    public Dropdown dropdown;

    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        PopulateList();
        Color tan = new Color(0.9f, 0.85f, 0.7f, 1);
        Color gray = new Color(.8f, .8f, .8f, 1);
        Color blue = new Color(.52f, .63f, .69f, 1);
        Color green = new Color(.67f, .74f, .6f, 1);
        Color red = new Color(.75f, .44f, .42f, 1);
        Color yellow = new Color(.97f, .95f, .72f, 1);
        colors = new List<Color>() { tan, Color.white, gray, blue, green, yellow, red };
        
    }

    public void ChangeBackground(int index)
    {
        cam.backgroundColor = colors[index];
    }

    void PopulateList()
    {
        dropdown.AddOptions(colorNames);
    }
}
